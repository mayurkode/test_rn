import formatAddress from "../formatAddress";

describe("formatAddress", () => {
  it("returns the formatted address", () => {
    const addressObject = {
      street1: "123 Main Street",
      street2: "Apartment 456",
      city: "Atlanta",
      state: "GA",
      zip: "30307",
    };

    const result = formatAddress(addressObject);
    const expected = "123 Main Street\nApartment 456\nAtlanta, GA 30307";
    // console.log(expected);
    expect(result).toEqual(expected);
  });

  it("see if street 1 line is string", () => {
    const addressObject = {
      street1: "123 Main Street",
      city: "Atlanta",
      state: "GA",
      zip: "30307",
    };

    const result = formatAddress(addressObject);
    const expected = "123 Main Street\nAtlanta, GA 30307";
    // console.log(expected);
    expect(result).toEqual(expected);
  });

  it("see if street 1 line is string", () => {
    const addressObject = {
      street1: "123 Main Street",
      city: "Atlanta",
      state: "GA",
      zip: "30307",
    };

    const result = formatAddress(addressObject);
    const expected = "123 Main Street\nAtlanta, LA 30307";
    // console.log(expected);
    expect(result).toEqual(expected);
  });
});
